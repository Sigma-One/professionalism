package sigmaone.professionalism.util;

import com.google.common.collect.ImmutableSet;
import net.fabricmc.fabric.api.object.builder.v1.trade.TradeOfferHelper;
import net.fabricmc.fabric.api.object.builder.v1.world.poi.PointOfInterestHelper;
import net.fabricmc.fabric.mixin.object.builder.VillagerProfessionAccessor;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.village.TradeOffers;
import net.minecraft.village.VillagerProfession;
import net.minecraft.world.poi.PointOfInterestType;
import org.jetbrains.annotations.Nullable;
import sigmaone.professionalism.Professionalism;

public class RegistryHelper {
    public static PointOfInterestType registerPOI(String id, Block[] blocks) {
        return PointOfInterestHelper.register(new Identifier(Professionalism.MOD_ID, id), 1, 10, blocks);
    }

    public static VillagerProfession registerProfession(
        String id,
        Block jobsite,
        @Nullable ImmutableSet<Item> gatherables,
        @Nullable ImmutableSet<Block> secondaryJobsites,
        @Nullable SoundEvent workSound
    ) {
        if (gatherables == null) {
            gatherables = ImmutableSet.of();
        }
        if (secondaryJobsites == null) {
            secondaryJobsites = ImmutableSet.of();
        }
        VillagerProfession profession = VillagerProfessionAccessor.create(
                Professionalism.MOD_ID + ":" + id,
                registerPOI(id, new Block[]{jobsite}),
                gatherables,
                secondaryJobsites,
                workSound
        );
        return Registry.register(Registry.VILLAGER_PROFESSION, Professionalism.MOD_ID + ":" + id, profession);
    }

    public static void registerTradeOffer(VillagerProfession job, int level, TradeOffers.Factory offer) {
        TradeOfferHelper.registerVillagerOffers(job, level, factories -> { factories.add(offer); });
    }

    public static void addBuyForOneEmeraldTrade(VillagerProfession job, Item item, int amount, int level, int stock, int experience) {
        TradeOffers.Factory factory = new TradeOffers.BuyForOneEmeraldFactory(item, amount, stock, experience);
        registerTradeOffer(job, level, factory);
    }

    public static void addSellItemTrade(VillagerProfession job, Item item, int price, int amount, int level, int stock, int experience) {
        TradeOffers.Factory factory = new TradeOffers.SellItemFactory(item, price, amount, stock, experience);
        registerTradeOffer(job, level, factory);
    }
}