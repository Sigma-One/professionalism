package sigmaone.professionalism;

import net.fabricmc.api.ModInitializer;
import net.minecraft.block.Blocks;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundEvents;
import net.minecraft.village.VillagerProfession;
import sigmaone.professionalism.util.RegistryHelper;

public class Professionalism implements ModInitializer {
    public static String MOD_ID = "professionalism";
    @Override
    public void onInitialize() {
        VillagerProfession DEBUGGER = RegistryHelper.registerProfession(
                "debugger",
                Blocks.COMMAND_BLOCK,
                null,
                null,
                null
        );
        RegistryHelper.addSellItemTrade(DEBUGGER, Items.COMMAND_BLOCK, 1, 1, 1, 16, 10);
        RegistryHelper.addSellItemTrade(DEBUGGER, Items.CHAIN_COMMAND_BLOCK, 2, 1, 2, 16, 10);
        RegistryHelper.addSellItemTrade(DEBUGGER, Items.REPEATING_COMMAND_BLOCK, 3, 1, 3, 16, 10);

        VillagerProfession ENGINEER = RegistryHelper.registerProfession(
                "engineer",
                Blocks.PISTON,
                null,
                null,
                SoundEvents.BLOCK_DISPENSER_FAIL
        );
        RegistryHelper.addBuyForOneEmeraldTrade(ENGINEER, Items.REDSTONE, 32, 1, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.REPEATER, 1, 2, 2, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.COMPARATOR, 2, 2, 2, 16,  10);
        RegistryHelper.addBuyForOneEmeraldTrade(ENGINEER, Items.REDSTONE_BLOCK, 3, 2, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.REDSTONE_LAMP, 2, 1, 2, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.DISPENSER, 1, 1, 3, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.DROPPER, 1, 1, 3, 16, 10);
        RegistryHelper.addBuyForOneEmeraldTrade(ENGINEER, Items.PISTON, 1, 3, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.STICKY_PISTON, 2, 1, 3, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.SLIME_BLOCK, 3, 1, 4, 16, 10);
        RegistryHelper.addSellItemTrade(ENGINEER, Items.OBSERVER, 2, 1, 4, 16, 10);
    }
}
